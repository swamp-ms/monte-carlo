# Requirements
* NodeJS 16 or higher
* NPM 8 (or higher)

# How to install
Using your installed NPM, run `npm i`

# How to configure
At the top of `sim.ts` you will find the following code:
```typescript
import {probabilities_t1 as distribution} from "./t1"
import {probabilities_t2} from "./t2"
import {probabilities_t3} from "./t3"
import {probabilities_t4} from "./t4"
import {probabilities_t5} from "./t5"
```

Note the `probabilities_t1 as distribution`. This specifies that we will run the 
simulation using the probabilities from P<sub>T1</sub>. To select, for instance, P<sub>T3</sub>
change the code to:

```typescript
import {probabilities_t1} from "./t1"
import {probabilities_t2} from "./t2"
import {probabilities_t3 as distribution} from "./t3"
import {probabilities_t4} from "./t4"
import {probabilities_t5} from "./t5"
```

# How to run
Run `npm run sim`
