import {MonomerGeneratorInterface} from "./MonomerGeneratorInterface";
import {VariantSpec} from "../model/VariantSpec";
import {flipCoin} from "../random/fipCoin";

export class RandomGenerator  implements MonomerGeneratorInterface {
    getMonomerString(spec: VariantSpec): string {
        let aCounter = spec.A;
        let bCounter = spec.B;
        let output = "";
        do {

            //add A or B
            if (flipCoin()) {
                //A
                if (aCounter > 0) {
                    if (flipCoin()) {
                        //prepend
                        output = "A" + output;
                    } else {
                        //append
                        output = output + "A";

                    }
                    aCounter--;
                }
            } else {
                //B
                if (bCounter > 0) {
                    if (flipCoin()) {
                        //prepend

                        output = "B" + output;

                    } else {
                        //append

                        output = output + "B";

                    }
                    bCounter--;
                }

            }

        } while (aCounter > 0 || bCounter > 0);
        return output;
    }

}

