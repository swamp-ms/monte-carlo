import {VariantSpec} from "../model/VariantSpec";

export interface MonomerGeneratorInterface {
    getMonomerString(spec: VariantSpec): string;
}
