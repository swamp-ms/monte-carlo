import {MonomerGeneratorInterface} from "./MonomerGeneratorInterface";
import {VariantSpec} from "../model/VariantSpec";
import {flipCoin} from "../random/fipCoin";

export class FixedGenerator implements MonomerGeneratorInterface {
    getMonomerString(spec: VariantSpec): string {

        const aStr = ''.padEnd(spec.A, 'A');
        const bStr = ''.padEnd(spec.B, 'B');
        if (flipCoin()) {
            return `${aStr}${bStr}`
        } else {
            return `${bStr}${aStr}`
        }
    }

}
