import {VariantSpec} from "../model/VariantSpec";

/**
 * Select a random variantSpec
 */
export class FragmentSpecGenerator {
    private distribution: Record<string, number>;

    constructor(distribution: Record<string, number>) {
        this.distribution = distribution;
    }

    /**
     * Returns a random fragment based on the probability distribution imported
     * Selection happens by segmenting the interval [0,1) into segments of length p (where p is the probability) then
     * randomly selecting a point on the interval [0,1) and determining which segment that point is in.
     */
    private randomEntry(): string {
        const selector = Math.random();
        let sum = 0.0;

        const result = Object.entries(this.distribution).find(([variant, p]) => {
            //console.debug(`?? ${sum} <= ${selector} < ${sum + p} //+${p}`);
            try {
                //@implNote: sum <= selector < sum + p
                //@implNote: because left-bound (and 0) are inclusive, if p1=0.15, then 0.15 is actually part of p2
                return sum <= selector && selector < sum + p;
            } finally {
                sum += p;
            }
        });

        const [variant,] = result;
        return variant;
    }

    private parseSpec(variant: string): VariantSpec {
        const parts = variant.split(":");
        const spec = {
            N: parseInt(parts[1]),
            A: parseInt(parts[2]),
            B: parseInt(parts[3])
        }

        if (spec.N !== spec.A + spec.B) {
            console.log(variant, spec);
            process.exit(1);
        }

        return spec;
    }


    randomSpec(): VariantSpec {
        const entry = this.randomEntry();
        return this.parseSpec(entry);
    }
}
