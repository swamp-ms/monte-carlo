/**
 * Flips a coin and returns true 50% of the time and false 50% of the time
 * @return true for head, false for tails
 */
export const flipCoin = (): boolean => {
    const random = Math.random();
    //note that this should be < and not <=
    //because Math.random is inclusive 0.0 and exclusive 1.0
    //this way the probability of heads is 0.5 and tails is 0.5
    //if <= is used, the probablity of heads would be larger
    return random < 0.5;
}
