/**
 * A VariantSpec defines how many A's and B's should be in a fragment
 */
export interface VariantSpec {
    N: number;
    A: number;
    B: number;
}
