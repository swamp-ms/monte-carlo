export interface MoleculeGeneratorInterface {
    build(): string
}
