import {MonomerGeneratorInterface} from "../fragmentGenerator/MonomerGeneratorInterface";
import {FragmentSpecGenerator} from "../Selector/FragmentSpecGenerator";

export class DefaultPolymerGenerator {
    private selector: FragmentSpecGenerator;
    private fragmentBuilder: MonomerGeneratorInterface;
    private numMonomers: number;

    constructor(selector: FragmentSpecGenerator, fragmentGenerator: MonomerGeneratorInterface, numMonomers: number = 100000) {
        this.selector = selector;
        this.fragmentBuilder = fragmentGenerator;
        this.numMonomers = numMonomers;
    }

    build(): string {
        const out: string[] = [];
        for (let i = 0; i < this.numMonomers; i++) {
            //select a C:N:A:B variant
            const randomFragmentSpec = this.selector.randomSpec();
            //monte-carlo the actual fragment
            const randomFragment = this.fragmentBuilder.getMonomerString(randomFragmentSpec);


            //see if we can append that fragment
            if (this.canAppend(out, randomFragment)) {

                if (randomFragment.length > 0) {
                    out.push(randomFragment.substring(1))
                }
            }
            //@todo: invert the fragment, and then append
            else {
                // NOP by design!
            }


        }

        return out.join("");
    }

    /**
     * true iff the fragment can be appended to the polymer
     * @param polymer
     * @param fragment
     */
    canAppend(polymer: string[], fragment: string) {
        if (polymer.length === 0) {
            return true;
        } else {
            //last monomer of polymer
            const tailFragment = polymer[polymer.length - 1]
            const lastMonomer = tailFragment[tailFragment.length - 1];

            //first monomer of attachment candidate
            const head = fragment[0];

            //if we start with the head
            if (head === lastMonomer) {
                return true;
            }
            //@todo: perhaps I can add the monomer rotated 180deg.
            else {
                //NOP BY DESIGN
            }
        }

        return false;
    }
}
