import {probabilities_t1} from "./data/t1"
import {probabilities_t2} from "./data/t2"
import {probabilities_t3} from "./data/t3"
import {probabilities_t4} from "./data/t4"
import {probabilities_t5} from "./data/t5"
import {VariantSpec} from "./src/model/VariantSpec";
import {DefaultPolymerGenerator} from "./src/moleculeGenerator/DefaultPolymerGenerator";
import {FixedGenerator} from "./src/fragmentGenerator/FixedGenerator";
import {FragmentSpecGenerator} from "./src/Selector/FragmentSpecGenerator";
import {RandomGenerator} from "./src/fragmentGenerator/RandomGenerator";


/**
 * The entrypoint of the simulation script
 * This is the primary method run
 */
const main = (selectedDist?: any) => {
    let finalADist = {};
    let finalBDist = {};
    let abDist = {};

    const moleculeGenerator = new DefaultPolymerGenerator(
        //
        new FragmentSpecGenerator(selectedDist),
        new RandomGenerator()
        //new FixedGenerator()
    )

    console.log(new Date());
    for (let j = 0; j < 100; j++) {


        const polymer = moleculeGenerator.build();
        process.stdout.write(".");

        const dist = getLengthDistribution(polymer);


        finalADist = addDist(finalADist, dist.aDist);
        finalBDist = addDist(finalBDist, dist.bDist);

        abDist = addDist(abDist, getAbTransitions(polymer))


    }
    console.log(``);
    console.log(`########################################`);
    console.log(`########################################`);
    console.log(abDist);
    console.log(`########################################`);
    console.log(`########################################`);
    dumpDists(finalADist, finalBDist);
    console.log(new Date());
}



const getRandomFragment = (distribution: Record<string, number>): string => {
    const selector = Math.random();
    let sum = 0.0;

    const result = Object.entries(distribution).find(([variant, p]) => {
        //console.debug(`?? ${sum} <= ${selector} < ${sum + p} //+${p}`);
        try {
            //@implNote: sum <= selector < sum + p
            //@implNote: because left-bound (and 0) are inclusive, if p1=0.15, then 0.15 is actually part of p2
            return sum <= selector && selector < sum + p;
        } finally {
            sum += p;
        }
    });

    const [variant,] = result;

    //once we have a variant spec, turn it into a fragment
    const variantSpec = variantToSpec(variant);
    const fragment = generateString(variantSpec.A, variantSpec.B);

    return fragment;
}

/**
 * Generate a random string of A's and B's based on the number of A's and B's
 * @param a The number of A's
 * @param b The number of B's
 */
const generateString = (a: number, b: number) => {
    let output = "";
    let aCounter = a;
    let bCounter = b;

    do {

        //add A or B
        if (flipCoin()) {
            //A
            if (aCounter > 0) {
                if (flipCoin()) {
                    //prepend
                    output = "A" + output;
                } else {
                    //append
                    output = output + "A";

                }
                aCounter--;
            }
        } else {
            //B
            if (bCounter > 0) {
                if (flipCoin()) {
                    //prepend

                    output = "B" + output;

                } else {
                    //append

                    output = output + "B";

                }
                bCounter--;
            }

        }

    } while (aCounter > 0 || bCounter > 0);
    return output;

}

/**
 * Flips a coin and returns true 50% of the time and false 50% of the time
 * @return true for head, false for tails
 */
const flipCoin = (): boolean => {
    const random = Math.random();
    //note that this should be < and not <=
    //because Math.random is inclusive 0.0 and exclusive 1.0
    //this way the probability of heads is 0.5 and tails is 0.5
    //if <= is used, the probablity of heads would be larger
    return random < 0.5;
}

/**
 * Turns a variantString (from a probability distribution e.g. t_1) C:2:1:0 into the corresponding fragment (e.g. {A:1,B:0})
 * @deprecated
 * @param variant The input string, e.g. C:4:2:2 (stating size=4, A=2, B=2)
 * @return A javascript-object representing the given variant
 */
const variantToSpec = (variant: string): VariantSpec => {
    const parts = variant.split(":");
    return {
        N: parseInt(parts[1]),
        A: parseInt(parts[2]),
        B: parseInt(parts[3])
    }
}

/**
 * @deprecated
 * Returns a random polymer based on the currently loaded probability  distribution script
 */
const getRandomPolymer = (selectedDist: Record<string, number>) => {
    let sequence = getRandomFragment(selectedDist);
    const n = 100000;
    for (let i = 0; i < n; i++) {
        const nextPart = getRandomFragment(selectedDist);
        const head = nextPart[0];

        //if we start with the head
        if (sequence.lastIndexOf(head) === sequence.length - 1) {
            sequence = `${sequence}${nextPart.substring(1)}`;
        } else {
            //skip by design
        }
    }
    return sequence;
}

/**
 * For output purposses, produces a distribution of how often a particular-length-chain of A's and B's occurs
 *
 * e.g. for AAAAABBBABAABBBA would produce something similar to:
 * {
 *     A : {
 *         1: 2
 *         2: 1
 *         5: 1
 *     },
 *     B : {
 *         1: 1
 *         2: 1,
 *         3 : 1
 *     }
 * }
 * E.g. the a: {...,5:1} means that there is 1 occurence of 5 successive A's (i.e. AAAAA)
 * @param polymer
 */
const getLengthDistribution = (polymer: string) => {
    const matches = [...polymer.matchAll(/A+|B+/g)];
    let As: Record<number, number> = {};
    let Bs: Record<number, number> = {};
    matches.forEach(([match]) => {
        if (match[0] === 'A') {
            As[match.length] = (As[match.length] ?? 0) + 1;
        } else {
            Bs[match.length] = (Bs[match.length] ?? 0) + 1;
        }
    });


    return {
        aDist: As,
        bDist: Bs
    };
}

/**
 * Dump the A and B distributions
 * @param aDist
 * @param bDist
 */
const dumpDists = (aDist, bDist) => {
    const max = Math.max(
        Math.max(...Object.keys(aDist).map((e) => parseInt(e))),
        Math.max(...Object.keys(bDist).map((e) => parseInt(e)))
    );
    for (let i = 0; i < max; i++) {
        const a = i * (aDist[`${i}`] ?? 0);
        const b = i * (bDist[`${i}`] ?? 0);
        const ab = a + b;
        console.log(`${i}\t${a}\t${b}\t${ab}`);
    }
}

/**
 * Add two distributions together pair-wise and returns the corresponding distribution
 * e.g. {A:1, B:1} + {B:1, C:1} would produce: {A:1,B:2,C:1}
 * @param distA
 * @param distB
 */
const addDist = (distA, distB) => {
    const keys = [
        ...Object.keys(distA),
        ...Object.keys(distB)
    ].sort();


    return Object.fromEntries(keys.map((key) => {
        return [
            key,
            (distA[key] ?? 0) + (distB[key] ?? 0)
        ]
    }));
}


/**
 * Returns the number of AA, AB, BA, and BB transitions
 * @param polymer
 */
const getAbTransitions = (polymer: string) => {
    const out: Record<string, number> = {
        'AA': 0,
        'AB': 0,
        'BA': 0,
        'BB': 0
    };
    for (let i = 1; i < polymer.length; i++) {
        const prev = polymer[i - 1];
        const curr = polymer[i];
        out[`${prev}${curr}`]++;
    }
    return out;
}

//invoke the main method
console.log("T1");
main(probabilities_t1);
console.log("T2");
main(probabilities_t2);
console.log("T3");
main(probabilities_t3);
console.log("T4");
main(probabilities_t4);
console.log("T5");
main(probabilities_t5);
